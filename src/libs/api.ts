import type { ChatMessage } from "@/types";

export async function api(body: string) {
    try {
        const result = await fetch("http://123.56.237.145:7777/chat/knowledge_base_chat", {
            method: "post",
            // signal: AbortSignal.timeout(8000),
            // 开启后到达设定时间会中断流式输出
            headers: {
                "Content-Type": "application/json",
            },
            body: body,
        });
        return result;
    } catch (error) {
        throw error;
    }
}
